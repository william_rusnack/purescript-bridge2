module Test.Utils where

import Data.Either
import Test.Hspec

shouldBeRight :: forall a b. (Show a, Show b) => Either a b -> Expectation
shouldBeRight = (`shouldSatisfy` isRight)
