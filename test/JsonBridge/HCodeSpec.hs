module JsonBridge.HCodeSpec where

import JsonBridge
import JsonBridge.HCodeTypes
import JsonBridge.HCodeInstances

import Test.Hspec

default (String)

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "HEncode HDecode" $ do
    describe "One" $ encodeDecode OneCon (JsonRep "One" "OneCon" [])

    describe "Two" $ do
      describe "Two0" $ encodeDecode Two0 (JsonRep "Two" "Two0" [])
      describe "Two1" $ encodeDecode Two1 (JsonRep "Two" "Two1" [])

    describe "Record1" $ do
      encodeDecode (Record1Con 1)
                   (JsonRep "Record1" "Record1Con" [JsonPrim "Int" "1"])

    describe "Record2" $ do
      encodeDecode (Record2Con 2 "hi")
                   (JsonRep "Record2" "Record2Con" [ JsonPrim "Int" "2"
                                                   , JsonPrim "String" "\"hi\"" ] )

encodeDecode d r = do
  it "hEncode" $ hEncode d `shouldBe` r
  it "hDecode" $ hDecode r `shouldBe` Right d
  it "hEncode then hDecode" $ (hDecode $ hEncode d) `shouldBe` Right d
  it "aeson encode decode" $ (decode $ encode d) `shouldBe` Right d
