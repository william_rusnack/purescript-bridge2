module JsonBridge.HCodeInstances where

import JsonBridge
import JsonBridge.HCodeTypes

import Control.Applicative.HT
import Data.Either.Combinators
import Data.Aeson
import GHC.Exts

$(thHEncodePrim primsToEncode)
$(thHDecodePrim primsToEncode)

$(thHEncode typesToEncode)
$(thHDecode typesToEncode)


