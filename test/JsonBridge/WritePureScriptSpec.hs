module JsonBridge.WritePureScriptSpec where

import JsonBridge.WritePureScript

import Language.PureScript.CST

import Test.Hspec
import Test.Utils

spec :: Spec
spec = do
  it "psCode" $ do
    shouldBeRight psCode
    let Right code = psCode
    shouldBeRight $ parse code

