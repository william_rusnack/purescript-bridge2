module Transpile2PureScript.Types where

import Transpile2PureScript
import Data.Text

default (Text)


data One = OneCon deriving (Show, Eq)
data Two = Two0 | Two1 deriving (Show, Eq)
data Record1 = Record1Con Int deriving (Show, Eq)
data Record2 = Record2Con {getRecord20 :: Int, getRecord21 :: String} deriving (Show, Eq)

rq = renderQ "Test.Rendering" [] []

