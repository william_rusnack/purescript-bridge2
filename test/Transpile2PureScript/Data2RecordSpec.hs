module Transpile2PureScript.Data2RecordSpec where

import Transpile2PureScript

import GHC.Generics
import Data.Either
import Test.Hspec
import Language.PureScript.CST


main :: IO ()
main = hspec spec

spec :: Spec
spec = describe "Data2RecordSpec" $ do
  it "SingleRec" $ do
    let code = translate @SingleRec
    code `shouldBe` "newtype SingleRec = SingleRec {getS :: Int}"
    shouldBeRight $ parse $ modulerize code

  it "DoubleRec" $ do
    let code = translate @DoubleRec
    code `shouldBe` "newtype DoubleRec = DoubleRec {getD0 :: String, getD1 :: Number}"
    shouldBeRight $ parse $ modulerize code

  it "TripleRec" $ do
    let code = translate @TripleRec
    code `shouldBe` "newtype TripleRec = TripleRec {getT0 :: String, getT1 :: Int, getT2 :: Number}"
    shouldBeRight $ parse $ modulerize code

data SingleRec = SingleRec {getS :: Int} deriving Generic
data DoubleRec = DoubleRec {getD0 :: String, getD1 :: Double} deriving Generic
data TripleRec = TripleRec {getT0 :: String, getT1 :: Int, getT2 :: Double} deriving Generic

modulerize = (<>) "module Data2RecordSpec where\n"

shouldBeRight :: forall a b. (Show a, Show b) => Either a b -> Expectation
shouldBeRight = (`shouldSatisfy` isRight)


