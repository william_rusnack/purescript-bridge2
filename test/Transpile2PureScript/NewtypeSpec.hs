module Transpile2PureScript.NewtypeSpec where

import Transpile2PureScript

import Test.Hspec

import GHC.Generics

main :: IO ()
main = hspec spec

spec :: Spec
spec = describe "Newtype" $ do
  it "SimpleNewtype" $
    translate @SimpleNewtype `shouldBe` "newtype SimpleNewtype = SimpleNewtype String"

  it "AccessorNewtype" $
    translate @AccessorNewtype `shouldBe` "newtype AccessorNewtype = AccessorNewtype {getAccessorNewtype :: Int}"

  it "NonPrim" $
    translate @NonPrim `shouldBe` "newtype NonPrim = NonPrim (Maybe Number)"

  it "ContainedNewtype" $
    translate @ContainedNewtype `shouldBe` "newtype ContainedNewtype = ContainedNewtype {getContainedNewtype :: NonPrim}"

newtype SimpleNewtype = SimpleNewtype String deriving Generic
newtype AccessorNewtype = AccessorNewtype {getAccessorNewtype :: Int} deriving Generic
newtype NonPrim = NonPrim (Maybe Double) deriving Generic
instance H2Ps NonPrim where h2Ps = "NonPrim"
newtype ContainedNewtype = ContainedNewtype {getContainedNewtype :: NonPrim} deriving Generic

