module Transpile2PureScript.TH2PureScriptSpec where

import Transpile2PureScript.Types

import Transpile2PureScript
import JsonBridge

import Language.Haskell.TH
import Language.PureScript.CST

import Data.Bifoldable
import Data.Text as T
import Data.Either

import Test.Hspec
import Test.Utils


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "thHEncodePrim" $ do
    it "Int" $ rightness $(rq =<< thHEncodePrim [''Int])
    it "String" $ rightness $(rq =<< thHEncodePrim [''String])
    it "Char" $ rightness $(rq =<< thHEncodePrim [''Char])

  describe "thHDncodePrim" $ do
    it "Int" $ rightness $(rq =<< thHDecodePrim [''Int])
    it "String" $ rightness $(rq =<< thHDecodePrim [''String])
    it "Char" $ rightness $(rq =<< thHDecodePrim [''Char])

  describe "thHEncode" $ do
    it "One" $ rightness $(rq =<< thHEncode [''One])
    it "Two" $ rightness $(rq =<< thHEncode [''Two])
    it "Record1" $ rightness $(rq =<< thHEncode [''Record1])
    it "Record2" $ rightness $(rq =<< thHEncode [''Record2])

  describe "thHDncode" $ do
    it "One" $ rightness $(rq =<< thHDecode[''One])
    it "Two" $ rightness $(rq =<< thHDecode[''Two])
    it "Record1" $ rightness $(rq =<< thHDecode[''Record1])
    it "Record2" $ rightness $(rq =<< thHDecode[''Record2])

rightness :: Either String Text -> Expectation
rightness renderedResult = do
  shouldBeRight renderedResult
  let Right renderedText = renderedResult
  shouldBeRight $ parse renderedText


