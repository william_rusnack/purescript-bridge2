module Transpile2PureScript.DataSpec where

import Transpile2PureScript

import Test.Hspec

import GHC.Generics

main :: IO ()
main = hspec spec

spec :: Spec
spec = describe "DataSpec" $ do
  it "One" $
    translate @One `shouldBe` "data One = One"
  it "Two" $
    translate @Two `shouldBe` "data Two = Two0  | Two1 "

  it "TwoWithRecord" $
    translate @TwoWithRecord `shouldBe` "data TwoWithRecord = TWR0 Int | TWR1 {getTWR0 :: Number, getTWR1 :: String}"

data One = One deriving Generic
data Two = Two0 | Two1 deriving Generic
data TwoWithRecord = TWR0 Int | TWR1 {getTWR0 :: Double, getTWR1 :: String} deriving Generic
