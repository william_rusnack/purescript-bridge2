{-# LANGUAGE UndecidableInstances #-}

module Transpile2PureScript
  ( translate
  , renderQ
  , H2Ps(h2Ps)
  ) where

import Control.Arrow
import Control.Monad

import Data.Char (isAlpha)
import Data.HashSet as HS
import Data.List as L
import Data.String
import Data.Text as T
import Data.ByteString.Lazy (ByteString)
import Data.Maybe

import Language.Haskell.TH
import GHC.Generics
import GHC.TypeLits
import GHC.Exts
import PyF
import Data.Type.Bool
import Data.Type.Equality

translate :: forall a. (Generic a, BuildPS (Rep a)) => Text
translate = buildPS @(Rep a)

class BuildPS (a :: * -> *) where buildPS :: Text

-- 'True newtype
instance ( BuildPS f
         , KnownSymbol name
         ) => BuildPS (D1 ('MetaData name mod package 'True) f) where
  buildPS = [fmt|newtype {sv @name} = {buildPS @f}|]
-- 'False newtype
instance ( KnownSymbol name
         , BuildPS a
         , BuildPS b
         ) => BuildPS (D1 ('MetaData name mod package 'False) (a :+: b)) where
  buildPS = [fmt|data {sv @name} = {constructors}|]
    where constructors = buildPS @(a :+: b)
-- 'False newtype but is single constructor with record
instance ( KnownSymbol name
         , KnownSymbol consName
         , BuildPS selector
         ) => BuildPS (D1 ('MetaData name mod package 'False) (C1 ('MetaCons consName 'PrefixI 'True) selector)) where
  buildPS = [fmt|newtype {sv @name} = {sv @consName} {{{buildPS @selector}}}|]
-- 'False newtype but is single constructor without arguments
instance ( KnownSymbol name
         , KnownSymbol consName
         ) => BuildPS (D1 ('MetaData name mod package 'False) (C1 ('MetaCons consName 'PrefixI 'False) U1)) where
  buildPS = [fmt|data {sv @name} = {sv @consName}|]

-- 'True for record selectors
instance ( BuildPS f
         , KnownSymbol consName
         ) => BuildPS (C1 ('MetaCons consName 'PrefixI 'True) f) where
  buildPS = [fmt|{sv @consName} {{{buildPS @f}}}|]
-- 'False for record selectors
instance ( BuildPS f
         , KnownSymbol consName
         ) => BuildPS (C1 ('MetaCons consName 'PrefixI 'False) f) where
  buildPS = [fmt|{sv @consName} {buildPS @f}|]
-- -- for constructors without arguments
-- instance ( KnownSymbol consName
--          ) => BuildPS (C1 ('MetaCons consName 'PrefixI 'False) U1) where
--   buildPS = sv @consName

instance BuildPS U1 where buildPS = ""

instance (BuildPS a, BuildPS b) => BuildPS (a :+: b) where
  buildPS = [fmt|{buildPS @a} | {buildPS @b}|]

instance (KnownSymbol recordName, BuildPS f) => BuildPS (S1 ('MetaSel ('Just recordName) sourceUnpackedness sourceStrictness decidedStrictness) f) where
  buildPS = [fmt|{sv @recordName} :: {buildPS @f}|]
instance BuildPS f => BuildPS (S1 ('MetaSel 'Nothing sourceUnpackedness sourceStrictness decidedStrictness) f) where
  buildPS = buildPS @f

instance (BuildPS a, BuildPS b) => BuildPS (a :*: b) where
  buildPS = [fmt|{buildPS @a}, {buildPS @b}|]

instance H2Ps c => BuildPS (Rec0 c) where
  buildPS = h2Ps @c

sv :: forall n. KnownSymbol n => Text
sv = pack $ symbolVal' (proxy# :: Proxy# n)

class H2Ps a where h2Ps :: Text
instance {-# OVERLAPPING #-} H2Ps String where h2Ps = "String"
instance H2Ps Text where h2Ps = "String"
instance H2Ps ByteString where h2Ps = "String"
instance H2Ps Int where h2Ps = "Int"
instance H2Ps Double where h2Ps = "Number"
instance {-# OVERLAPPABLE #-} GetTypeName (Rep a) => H2Ps [a] where h2Ps = [fmt|List ({getTypeName @a})|]
instance H2Ps a => H2Ps (Maybe a) where h2Ps = [fmt|(Maybe {h2Ps @a})|]

getTypeName ::forall a. GetTypeName (Rep a) => Text
getTypeName = _getTypeName @(Rep a)
class GetTypeName (a :: * -> *) where _getTypeName :: Text
instance KnownSymbol n => GetTypeName (D1 ('MetaData n m p nt) c) where
  _getTypeName = sv @n


renderQ :: Text -> [Import] -> [Text] -> [Dec] -> ExpQ
renderQ moduleName imports extraCode =
  PureScript mempty >>>
  renderPS >>>
  runEither' >>>
  \case Left s -> [e|Left $(stringE s)|]
        Right templateCode -> let
            moduleLine = [fmt|module {moduleName} where|]
            importLines = fmap renderImport imports
            code = stringE $ unpack $ newlinize $ moduleLine : importLines <> extraCode
          in [e|Right $code|]

data Import = Import {getModule :: Text} -- , getImports :: Maybe [Text]}
instance IsString Import where fromString s = Import (pack s)
renderImport (Import moduleName) = [fmt|import {moduleName}|]
-- renderImport (Import moduleName (Just imports)) = [fmt|import moduleName 

renderPS :: PureScript -> Either' String Text
renderPS (PureScript l []) = Right' l
renderPS (PureScript l (d:ds)) = renderPS =<< (pure (PureScript l ds)  <> th2ps d)

data PureScript = PureScript {getCode :: Text, getDecs :: [Dec]}
  deriving (Eq)

pureps :: Text -> PureScript
pureps t = PureScript t mempty

instance Semigroup PureScript where
  (PureScript l ds) <> (PureScript l' ds') = PureScript (l <> l') (ds <> ds')

instance IsString PureScript where
  fromString s = PureScript (pack s) mempty

instance Monoid PureScript where
  mempty = PureScript mempty mempty

class TH2PureScript a where th2ps :: a -> Either' String PureScript

instance TH2PureScript Dec where
  -- FunD Name [Clause]
  th2ps (FunD name clauses) = do
    nameAndSpace <- th2ps name <> " "
    mconcat $ L.intersperse ("\n") $ (pure . (<>) nameAndSpace <=< th2ps) <$> clauses
  -- ValD Pat Body [Dec]
  th2ps (ValD _ _ _) = Left' "ValD not defined\n"
  -- DataD Cxt Name [TyVarBndr] (Maybe Kind) [Con] [DerivClause]
  th2ps (DataD _ _ _ _ _ _) = Left' "DataD not defined\n"
  -- NewtypeD Cxt Name [TyVarBndr] (Maybe Kind) Con [DerivClause]
  th2ps (NewtypeD _ _ _ _ _ _) = Left' "NewtypeD not defined\n"
  -- TySynD Name [TyVarBndr] Type
  th2ps (TySynD _ _ _) = Left' "TySynD not defined\n"
  -- ClassD Cxt Name [TyVarBndr] [FunDep] [Dec]
  th2ps (ClassD _ _ _ _ _) = Left' "ClassD not defined\n"
  -- InstanceD (Maybe Overlap) Cxt Type [Dec]
  th2ps (InstanceD (Just _) _ _ _) = Left' "Overlap for InstanceD not supported\n"
  th2ps (InstanceD _ c t ds) = do
    let typ = th2ps t
    tt <- typ
    ( "instance " <>
      (pure $ pureps $ toLower $ T.filter isAlpha $ getCode tt) <>
      " :: " <>
      th2ps c <>
      typ <>
      " where" <>
      (newlinize . indent 1 <$> (traverse th2ps ds)) )
  -- SigD Name Type-- ForeignD Foreign
  th2ps (SigD _ _) = Left' "SigD not defined\n"
  -- InfixD Fixity Name
  th2ps (InfixD _ _) = Left' "InfixD not defined\n"
  -- PragmaD Pragma
  th2ps (PragmaD _) = Left' "PragmaD not defined\n"
  -- DataFamilyD Name [TyVarBndr] (Maybe Kind)
  th2ps (DataFamilyD _ _ _) = Left' "DataFamilyD not defined\n"
  -- DataInstD Cxt Name [Type] (Maybe Kind) [Con] [DerivClause]
  th2ps (DataInstD _ _ _ _ _ _) = Left' "DataInstD not defined\n"
  -- NewtypeInstD Cxt Name [Type] (Maybe Kind) Con [DerivClause]
  th2ps (NewtypeInstD _ _ _ _ _ _) = Left' "NewtypeInstD not defined\n"
  -- TySynInstD Name TySynEqn
  th2ps (TySynInstD _ _) = Left' "TySynInstD not defined\n"
  -- OpenTypeFamilyD TypeFamilyHead
  th2ps (OpenTypeFamilyD _) = Left' "OpenTypeFamilyD not defined\n"
  -- ClosedTypeFamilyD TypeFamilyHead [TySynEqn]
  th2ps (ClosedTypeFamilyD _ _) = Left' "ClosedTypeFamilyD not defined\n"
  -- RoleAnnotD Name [Role]
  th2ps (RoleAnnotD _ _) = Left' "RoleAnnotD not defined\n"
  -- StandaloneDerivD (Maybe DerivStrategy) Cxt Type
  th2ps (StandaloneDerivD _ _ _) = Left' "StandaloneDerivD not defined\n"
  -- DefaultSigD Name Type
  th2ps (DefaultSigD _ _) = Left' "DefaultSigD not defined\n"
  -- PatSynD Name PatSynArgs PatSynDir Pat
  th2ps (PatSynD _ _ _ _) = Left' "PatSynD not defined\n"
  -- PatSynSigD Name PatSynType
  th2ps (PatSynSigD _ _) = Left' "PatSynSigD not defined\n"

instance TH2PureScript Clause where
  -- Clause [Pat] Body [Dec]
  th2ps (Clause ps b ds) = (spacize <$> traverse th2ps ps) <>
                           ( case ps of [] -> "= "
                                        _ -> " = " ) <>
                           th2ps b <>
                           case ds of
                             [] -> ""
                             _ -> "\n  where" <>
                                  (mconcat . indent 4 <$> traverse th2ps ds)

instance TH2PureScript Pat where
  -- LitP Lit
  th2ps (LitP x) = th2ps x
  -- VarP Name
  th2ps (VarP x) = th2ps x
  -- TupP [Pat]
  th2ps (TupP _) = Left' "TupP not defined\n"
  -- UnboxedTupP [Pat]
  th2ps (UnboxedTupP _) = Left' "UnboxedTupP not defined\n"
  -- UnboxedSumP Pat SumAlt SumArity
  th2ps (UnboxedSumP _ _ _) = Left' "UnboxedSumP not defined\n"
  -- ConP Name [Pat]
  th2ps (ConP n ps) = "(" <> th2ps n <> " " <> (spacize <$> traverse th2ps ps) <> ")"
  -- InfixP Pat Name Pat
  th2ps (InfixP _ _ _) = Left' "InfixP not defined\n"
  -- UInfixP Pat Name Pat
  th2ps (UInfixP _ _ _) = Left' "UInfixP not defined\n"
  -- ParensP Pat
  th2ps (ParensP _) = Left' "ParensP not defined\n"
  -- TildeP Pat
  th2ps (TildeP _) = Left' "TildeP not defined\n"
  -- BangP Pat
  th2ps (BangP _) = Left' "BangP not defined\n"
  -- AsP Name Pat
  th2ps (AsP _ _) = Left' "AsP not defined\n"
  -- WildP
  th2ps (WildP) = "_"
  -- RecP Name [FieldPat]
  th2ps (RecP _ _) = Left' "RecP not defined\n"
  -- ListP [ Pat ]
  th2ps (ListP ps) = listize <$> traverse th2ps ps
  -- SigP Pat Type
  th2ps (SigP _ _) = Left' "SigP not defined\n"
  -- ViewP Exp Pat
  th2ps (ViewP _ _) = Left' "ViewP not defined\n"

instance TH2PureScript Name where
  th2ps = pure . fromString . nameBase

instance TH2PureScript Body where
  -- GuardedB [(Guard, Exp)]
  th2ps (GuardedB _) = Left' "GuardedB not defined\n"
  -- NormalB Exp
  th2ps (NormalB x) = th2ps x

instance TH2PureScript Exp where
  -- VarE Name
  th2ps (VarE x) = th2ps x
  -- ConE Name
  th2ps (ConE x) = th2ps x
  -- LitE Lit
  th2ps (LitE x) = th2ps x
  -- AppE Exp Exp
  th2ps (AppE x y) = th2ps x <> " (" <> th2ps y <> ")"
  -- AppTypeE Exp Type
  th2ps (AppTypeE _ _) = Left' "AppTypeE not defined\n"
  -- InfixE (Maybe Exp) Exp (Maybe Exp)
  th2ps (InfixE x i y) = maybe "(_" th2ps x <>
                         " " <> infixEConversion (th2ps i) <> " " <>
                         maybe "_)" th2ps y
  -- UInfixE Exp Exp Exp
  th2ps (UInfixE _ _ _) = Left' "UInfixE not defined\n"
  -- ParensE Exp
  th2ps (ParensE _) = Left' "ParensE not defined\n"
  -- LamE [Pat] Exp
  th2ps (LamE _ _) = Left' "LamE not defined\n"
  -- LamCaseE [Match]
  th2ps (LamCaseE _) = Left' "LamCaseE not defined\n"
  -- TupE [Exp]
  th2ps (TupE _) = Left' "TupE not defined\n"
  -- UnboxedTupE [Exp]
  th2ps (UnboxedTupE _) = Left' "UnboxedTupE not defined\n"
  -- UnboxedSumE Exp SumAlt SumArity
  th2ps (UnboxedSumE _ _ _) = Left' "UnboxedSumE not defined\n"
  -- CondE Exp Exp Exp
  th2ps (CondE _ _ _) = Left' "CondE not defined\n"
  -- MultiIfE [(Guard, Exp)]
  th2ps (MultiIfE _) = Left' "MultiIfE not defined\n"
  -- LetE [Dec] Exp
  th2ps (LetE _ _) = Left' "LetE not defined\n"
  -- CaseE Exp [Match]
  th2ps (CaseE _ _) = Left' "CaseE not defined\n"
  -- DoE [Stmt]
  th2ps (DoE _) = Left' "DoE not defined\n"
  -- CompE [Stmt]
  th2ps (CompE _) = Left' "CompE not defined\n"
  -- ArithSeqE Range
  th2ps (ArithSeqE _) = Left' "ArithSeqE not defined\n"
  -- ListE [ Exp ]
  th2ps (ListE es) = listize <$> traverse th2ps es
  -- SigE Exp Type
  th2ps (SigE _ _) = Left' "SigE not defined\n"
  -- RecConE Name [FieldExp]
  th2ps (RecConE _ _) = Left' "RecConE not defined\n"
  -- RecUpdE Exp [FieldExp]
  th2ps (RecUpdE _ _) = Left' "RecUpdE not defined\n"
  -- StaticE Exp
  th2ps (StaticE _) = Left' "StaticE not defined\n"
  -- UnboundVarE Name
  th2ps (UnboundVarE _) = Left' "UnboundVarE not defined\n"
  -- LabelE String
  th2ps (LabelE _) = Left' "LabelE not defined\n"

instance TH2PureScript Type where
  -- ForallT [TyVarBndr] Cxt Type
  th2ps (ForallT _ _ _) = Left' "ForallT not defined\n"
  -- AppT Type Type
  th2ps (AppT x w) = th2ps x <> " " <> th2ps w
  -- SigT Type Kind
  th2ps (SigT _ _) = Left' "SigT not defined\n"
  -- VarT Name
  th2ps (VarT _) = Left' "VarT not defined\n"
  -- ConT Name
  th2ps (ConT n) = th2ps n
  -- PromotedT Name
  th2ps (PromotedT _) = Left' "PromotedT not defined\n"
  -- InfixT Type Name Type
  th2ps (InfixT _ _ _) = Left' "InfixT not defined\n"
  -- UInfixT Type Name Type
  th2ps (UInfixT _ _ _) = Left' "UInfixT not defined\n"
  -- ParensT Type
  th2ps (ParensT _) = Left' "ParensT not defined\n"
  -- TupleT Int
  th2ps (TupleT _) = Left' "TupleT not defined\n"
  -- UnboxedTupleT Int
  th2ps (UnboxedTupleT _) = Left' "UnboxedTupleT not defined\n"
  -- UnboxedSumT SumArity
  th2ps (UnboxedSumT _) = Left' "UnboxedSumT not defined\n"
  -- ArrowT
  th2ps (ArrowT) = Left' "ArrowT not defined\n"
  -- EqualityT
  th2ps (EqualityT) = Left' "EqualityT not defined\n"
  -- ListT
  th2ps (ListT) = Left' "ListT not defined\n"
  -- PromotedTupleT Int
  th2ps (PromotedTupleT _) = Left' "PromotedTupleT not defined\n"
  -- PromotedNilT
  th2ps (PromotedNilT) = Left' "PromotedNilT not defined\n"
  -- PromotedConsT
  th2ps (PromotedConsT) = Left' "PromotedConsT not defined\n"
  -- StarT
  th2ps (StarT) = Left' "StarT not defined\n"
  -- ConstraintT
  th2ps (ConstraintT) = Left' "ConstraintT not defined\n"
  -- LitT TyLit
  th2ps (LitT _) = Left' "LitT not defined\n"
  -- WildCardT
  th2ps (WildCardT) = Left' "WildCardT not defined\n"

instance TH2PureScript Cxt where
  th2ps [] = ""
  th2ps ts = (tuplize <$> traverse th2ps ts) <> " => "

instance TH2PureScript Lit where
  -- CharL Char	
  th2ps (CharL _) = Left' "CharL not defined\n"
  -- StringL String	
  th2ps (StringL x) = th2ps x
  -- IntegerL Integer --	Used for overloaded and non-overloaded literals. We don't have a good way to represent non-overloaded literals at the moment. Maybe that doesn't matter?
  th2ps (IntegerL _) = Left' "IntegerL not defined\n"
  -- RationalL Rational	
  th2ps (RationalL _) = Left' "RationalL not defined\n"
  -- IntPrimL Integer	
  th2ps (IntPrimL _) = Left' "IntPrimL not defined\n"
  -- WordPrimL Integer	
  th2ps (WordPrimL _) = Left' "WordPrimL not defined\n"
  -- FloatPrimL Rational	
  th2ps (FloatPrimL _) = Left' "FloatPrimL not defined\n"
  -- DoublePrimL Rational	
  th2ps (DoublePrimL _) = Left' "DoublePrimL not defined\n"
  -- StringPrimL [Word8]	-- A primitive C-style string, type Addr#
  th2ps (StringPrimL _) = Left' "StringPrimL not defined\n"
  -- CharPrimL Char
  th2ps (CharPrimL _) = Left' "CharPrimL not defined\n"

instance TH2PureScript String where
  th2ps x = "\"" <> fromString (addBackSlash x) <> "\""
    

-- Compatability Conversions --

infixEConversion "." = "<<<"
infixEConversion x = x

-- Utilities --

tuplize :: [PureScript] -> PureScript
tuplize = containerize "(" ")" ", "

listize :: [PureScript] -> PureScript
listize = containerize "[" "]" ", "

spacize :: [PureScript] -> PureScript
spacize = containerize "" "" " "

containerize :: PureScript -> PureScript -> PureScript -> [PureScript] -> PureScript
containerize opener closer seperator ls =
  mconcat $ opener : (L.intersperse seperator ls) <> [closer]

newlinize :: (IsString a, Semigroup a, Monoid a) => [a] -> a
newlinize = mconcat . fmap (prefixWith "\n")

indent i = applyToLines $ prefixWith (T.replicate i "  ")

prefixWith p = (<>) p

class ApplyToLines a where
  applyToLines :: (Text -> Text) -> a -> a

instance ApplyToLines PureScript where
  applyToLines f ps@(PureScript l _) =
    ps {getCode = T.intercalate "\n" $ fmap f $ splitOn "\n" l}

instance (Functor f, ApplyToLines a) => ApplyToLines (f a) where
  applyToLines f xs = applyToLines f <$> xs

infixr 1 >$>
(>$>) :: Functor f => (a -> f b) -> (b -> c) -> (a -> f c)
f >$> f' = fmap f' . f

addBackSlash = mconcat . fmap (\c -> if member c backslashedChars then ['\\', c] else [c])
  where backslashedChars = HS.fromList ['\"']

-- Either' --

runEither' :: Either' a b -> Either a b
runEither' (Right' x) = Right x
runEither' (Left' x) = Left x

succeed :: b -> Either' a b
succeed = Right'

throwError :: a -> Either' a b
throwError = Left'

data Either' a b = Left' a | Right' b deriving (Show, Eq, Functor)
instance (Semigroup a, Semigroup b) => Semigroup (Either' a b) where
  (Right' x) <> (Right' y) = Right' $ x <> y
  (Left' x) <> (Left' y) = Left' $ x <> y
  x@(Left' _) <> _ = x
  _ <> y@(Left' _) = y
instance (Semigroup a, Monoid b) => Monoid (Either' a b) where
  mempty = Right' mempty
instance Semigroup a => Applicative (Either' a) where
  (Right' f) <*> (Right' v) = Right' $ f v
  (Left' x) <*> (Left' v) = Left' $ x <> v
  (Left' x) <*> _ = Left' x
  _ <*> (Left' x) = Left' x
  pure = Right'
instance Semigroup a => Monad (Either' a) where
  (Right' x) >>= f = f x
  (Left' x) >>= _ = Left' x
instance IsString b => IsString (Either' a b) where
  fromString = Right' . fromString
