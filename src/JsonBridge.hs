module JsonBridge where

import Control.Applicative
import Control.Applicative.HT
import Control.Arrow
import Control.Monad

import Data.Aeson as A
import Data.Bifunctor as BF
import Data.ByteString.Lazy as BS
import Data.Either.Combinators
import Data.Foldable as F
import Data.Functor
import Data.HashMap.Strict as HM
import Data.Hashable
import Data.Kind
import Data.Text as T
import GHC.Generics

import Language.Haskell.TH

import PyF


class HEncode a where
  encode :: a -> ByteString
  encode = A.encode . hEncode

  hEncode :: a -> JsonRep

class HDecode a where
  decode :: ByteString -> Either String a
  decode = hDecode <=< A.eitherDecode

  hDecode :: JsonRep -> Either String a

data JsonRep = JsonRep { getType :: Text
                       , getConstructor :: Text
                       , getArguments :: [JsonRep]
                       }
             | JsonPrim { getPrimType :: Text
                        , getValue :: ByteString
                        }
  deriving (Show, Generic)

instance Eq JsonRep where
  (JsonRep t c a) == (JsonRep t' c' a') = t == t' && c == c' && a == a'
  (JsonPrim t v) == (JsonPrim t' v') = t == t' && hash v == hash v'
  _ == _ = False

haskellTypeKey = "hsType"
purescriptTypeKey = "psType"
constructorKey = "constructor"
argumentsKey = "arguments"
valueKey = "value"

instance ToJSON JsonRep where
  toJSON (JsonRep t c as) = Object $ HM.fromList
    [ (haskellTypeKey, String t)
    , (purescriptTypeKey, String t)
    , (constructorKey, String c)
    , (argumentsKey, toJSON as)
    ]
  toJSON (JsonPrim t v) = Object $ HM.fromList
    [ (haskellTypeKey, String t)
    , (purescriptTypeKey, String t)
    , (valueKey, toJSON $ BS.unpack v)
    ]

instance FromJSON JsonRep where
  parseJSON (Object hm) = lift3 JsonRep 
                                (lookupString haskellTypeKey)
                                (lookupString constructorKey)
                                arguments <|>
                          lift2 JsonPrim
                                (lookupString haskellTypeKey)
                                value
    where lookupString = lookupValue >=> \case
            String v -> pure v
            v -> fail [fmt|Expecting a Value.String but recieved "{show v}"|]

          lookupValue k = case HM.lookup k hm of
            Just v -> pure v
            Nothing -> fail [fmt|When parsing JsonRep, the key "{show k}" could not be found|]

          arguments =
            lookupValue argumentsKey >>=
            (\case A.Array a -> pure a
                   _ -> fail [fmt|Expecting an "Array" for the "{argumentsKey}"|] ) >>=
            traverse parseJSON >>=
            pure . F.toList

          value = lookupValue valueKey >>=
            parseJSON >>=
            pure . BS.pack

  parseJSON _ = fail "Expecting an object when parsing JsonRep"


-- TemplateHaskell --

thHEncodePrim :: [Name] -> DecsQ
thHEncodePrim = traverse $ \typeName -> do
  let typeStr = litE $ name2str typeName
  let clauses = (: []) $ clause
        []
        (normalB [e|JsonPrim $typeStr . A.encode|])
        []

  instanceD (cxt []) (appT (conT ''HEncode) (conT typeName)) [funD 'hEncode clauses]

thHEncode :: [Name] -> DecsQ
thHEncode = traverse $ \typeName -> do
  cons <- conNameLen typeName
  let clauses = cons <&> \(conName, numArgs) -> do
        argNames <- replicateM numArgs $ newName "x"
        let typeStr = litE $ name2str typeName
        let conStr = litE $ name2str conName
        let jsonRepArgs = listE $ argNames <&> \an -> [e|hEncode $(varE an)|]
        clause [conP conName $ varP <$> argNames]
               (normalB $ [e|JsonRep $typeStr $conStr $jsonRepArgs|])
               []

  instanceD (cxt []) (appT (conT ''HEncode) (conT typeName)) [funD 'hEncode clauses]

thHDecodePrim :: [Name] -> DecsQ
thHDecodePrim = traverse $ \typeName -> do
  let typeStr = name2str typeName

  typeVar <- newName "typeStr"
  byteVar <- newName "rawBytes"

  let decodeError = [e|"Cannot decode ByteString " <> (show $(varE byteVar)) <> " to type " <> T.unpack $(litE typeStr) <> " with Data.Aeson.decode"|]

  let correctClauses =
        [ clause [[p|(JsonPrim $(litP typeStr) $(varP byteVar))|]]
                 (normalB [e|maybeToRight "Could not decode primitive bytestring" $ A.decode $(varE byteVar)|])
                 []
        ]

  let incorrectTypeError = [e|Left $ "Expecting \"" <> T.unpack $(litE typeStr) <> "\" but recived type \"" <> T.unpack $(varE typeVar) <> "\"."|]

  let incorrectTypeClauses =
        [ clause [[p|(JsonPrim $(varP typeVar) _)|]] (normalB incorrectTypeError) []
        , clause [[p|(JsonRep $(varP typeVar) _ _)|]] (normalB incorrectTypeError) []
        ]

  let clauses = correctClauses <> incorrectTypeClauses
  instanceD (cxt []) (appT (conT ''HDecode) (conT typeName)) [funD 'hDecode clauses]

thHDecode :: [Name] -> DecsQ
thHDecode = traverse $ \typeName -> do
  let typeStr = name2str typeName

  typeVar <- newName "typeStr"
  constructorVar <- newName "constructorStr"
  argumentsVar <- newName "arguments"

  cons <- conNameLen typeName
  let typeLit = litP typeStr
  let correctClauses = cons <&> \(conName, numArgs) -> do
        argNames <- replicateM numArgs $ newName "x"
        let listDeconstruct = listP $ varP <$> argNames
        let conExp = conE conName
        let constructor = case numArgs of
              0 -> [e|Right $conExp|]
              1 -> appE (varE 'lift) conExp
              _ -> appE (dyn $ "lift" <> show numArgs) conExp

        let decodedArgs = argNames <&> \an -> [e|(hDecode $(varE an))|]
        clause [[p|(JsonRep $typeLit $(litP $ name2str conName) $listDeconstruct)|]]
                  (normalB $ appMult constructor decodedArgs)
                  []

  let incorrectArgsClauses =
        [ clause [[p|(JsonRep $typeLit $(varP constructorVar) $(varP argumentsVar))|]]
                 (normalB [e|Left ("Incorrect type constructor \"" <> T.unpack $(varE constructorVar) <> "\" or number of arguments for constructor with " <> (show $ F.length $(varE argumentsVar)) <> " arguments given for type \"" <> T.unpack $(litE typeStr) <> "\".")|])
                 []
        ]

  let wrongTypeBody = normalB [e|Left ("Expecting type \"" <> T.unpack $(litE typeStr) <> "\" but recieved \"" <> T.unpack $(varE typeVar))|]
  let incorrectTypeClauses =
        [ clause [[p|(JsonRep $(varP typeVar) _ _)|]] wrongTypeBody []
        , clause [[p|(JsonPrim $(varP typeVar) _)|]] wrongTypeBody []
        ]

  let clauses = correctClauses <> incorrectArgsClauses <> incorrectTypeClauses
  instanceD (cxt []) (appT (conT ''HDecode) (conT typeName)) [funD 'hDecode clauses]


-- Utility Functions --

name2str = stringL . nameBase

conNameLen :: Name -> Q [(Name, Int)]
conNameLen =
  reify >=>
  ( \case
    TyConI d -> return d
    _ -> fail "Only works for TyConI"
    :: Info -> Q Dec ) >=>
  ( \case
    -- DataD Cxt Name [TyVarBndr] (Maybe Kind) [Con] [DerivClause]
    DataD _ _ _ _ cons _ -> return cons
    -- NewtypeD Cxt Name [TyVarBndr] (Maybe Kind) Con [DerivClause]
    NewtypeD _ _ _ _ con _ -> return [con]
    _ -> fail "Only works for DataD and NewtypeD"
    :: Dec -> Q [Con] ) >=>
  ( sequence . fmap (\case
    NormalC name types -> return (name, F.length types)
    RecC name types -> return (name, F.length types)
    _ -> fail "Only supports NormalC and RecC data constructors" )
    :: [Con] -> Q [(Name, Int)] )

appMult :: ExpQ -> [ExpQ] -> ExpQ
appMult = F.foldl' appE

