module JsonBridge.WritePureScript where

import JsonBridge
import JsonBridge.Types
import Transpile2PureScript

import Control.Monad
import Data.Text

psCode :: Either String Text
psCode = $(renderQ "JsonBridge" ["Data.List"] [translate @JsonRep] . join =<< sequence
    [ thHEncodePrim primTypes
    , thHDecodePrim primTypes
    , thHEncode types
    , thHDecode types
    ]
  )


writePS = case psCode of
  Right x -> flip writeFile $ unpack x
  Left x -> fail x

